# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'

    def get_move_line2(self, values):
        res = super(StatementLine, self).get_move_line2(values)
        account = values.get('account')
        if self.invoice and self.invoice.agent and account and account.party_required:
            res['party'] = self.invoice.agent.party.id
        return res
#
# class Journal(metaclass=PoolMeta):
#     __name__ = 'account.statement.journal'
#
#     @classmethod
#     def __setup__(cls):
#         super(Journal, cls).__setup__()
#         new_sel = [
#                 ('commission', 'Commission'),
#         ]
#         if new_sel not in cls.kind.selection:
#             cls.kind.selection.extend(new_sel)

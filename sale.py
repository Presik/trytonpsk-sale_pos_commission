# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import unicode_literals
import math
from datetime import date
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.model import fields
from trytond.wizard import Wizard

_ZERO = Decimal('0.0')


def round_floor(x):
    return int(math.floor(x / 100.0)) * 100


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    payable_no_commission = fields.Function(fields.Integer('Payable'),
        'get_payable_no_commission')

    @classmethod
    def do_reconcile(cls, sales):
        super(Sale, cls).do_reconcile(sales)
        for sale in sales:
            if sale.agent and sale.commission:
                if sale.commission > _ZERO:
                    cls.reconcile_sale_commission(sale)

    @classmethod
    def reconcile_sale_commission(cls, sale):
        MoveLine = Pool().get('account.move.line')
        accounts_rec = []
        lines_to_reconcile = []
        commission_payment = _ZERO
        for st_line in sale.payments:
            if st_line.statement.journal.kind == 'payment':
                commission_payment += st_line.amount
                for line in st_line.move.lines:
                    if line.account.id != st_line.account.id and not line.reconciliation:
                        lines_to_reconcile.append(line)
                        accounts_rec.append(line.account.id)
        if commission_payment != _ZERO:
            commissions_reconcile = cls.process_commission(sale, commission_payment, accounts_rec)
            if commissions_reconcile:
                lines_to_reconcile.extend(commissions_reconcile)
            if lines_to_reconcile:
                MoveLine.reconcile(lines_to_reconcile)

    @classmethod
    def process_commission(cls, sale, commission_payment, accounts_rec):
        lines_to_reconcile = []
        invoice = sale.invoices[0]
        Invoice = Pool().get('account.invoice')
        Commission = Pool().get('commission')

        commissions = Commission.search([
            ('origin', '=', str(invoice)),
            ('invoice_line.invoice.state', '!=', 'paid')

        ])
        if not commissions:
            return []
        commission = commissions[0]
        if commission_payment > 0:
            Commission.write(commissions, {'amount': commission_payment})
            Commission.invoice(commissions)
            commission.invoice_line.invoice.invoice_date = date.today()
            commission.invoice_line.invoice.payment_term = sale.payment_term.id
            commission.invoice_line.invoice.save()
            Commission.set_number_invoice(commission)
            Invoice.post([commission.invoice_line.invoice])
            for line in commission.invoice_line.invoice.move.lines:
                if line.account.type.payable:
                    lines_to_reconcile.append(line)
        return lines_to_reconcile

    def get_payable_no_commission(self, name=None):
        amount = 0
        if self.agent and self.commission:
            value = float(self.untaxed_amount) * self.commission / 100
            amount = int(self.total_amount - Decimal(round_floor(value)))
        return amount

    @classmethod
    def get_data(cls, args, context=None):
        res = super(Sale, cls).get_data(args, context)
        sale_id = args['sale_id']
        sale, = cls.browse([sale_id])
        if sale.agent and sale.commission and sale.payable_no_commission:
            res['pos_notes'] = 'CXP > ' + str(sale.payable_no_commission)
        return res

    @classmethod
    def get_extras(cls, sale):
        res = super(Sale, cls).get_extras(sale)
        if sale.agent and sale.commission and sale.payable_no_commission:
            res['pos_notes'] = 'CXP > ' + str(sale.payable_no_commission)
        return res


class SaleForceDraft(Wizard):
    __name__ = 'sale_pos.force_draft'

    def transition_force_draft(self):
        Commision = Pool().get('commission')
        Sale = Pool().get('sale.sale')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        sales = Sale.browse(ids)
        for sale in sales:
            if sale.invoices:
                for inv in sale.invoices:
                    commisions_ = Commision.search([
                        ('origin', '=', 'account.invoice,'+str(inv.id))
                    ])
                    Commision.delete(commisions_)
        super(SaleForceDraft, self).transition_force_draft()
        return 'end'
